# -*- coding: utf-8 -*-

from httplib import BadStatusLine
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver import FirefoxOptions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait
from time import sleep
import logging
import os

""" Skip alerts """
def skip_possible_alerts(driver):
    try:
        while expected_conditions.alert_is_present():
            alert = driver.switch_to.alert
            alert.accept()
    except NoAlertPresentException:
        pass

    return 0


PHONE = os.environ['PHONE']


""" Set loglevel """
logging.basicConfig(level=logging.DEBUG)

""" Set chrome options """
chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--allow-silent-push')
chrome_options.add_argument('--disable-notifications')
chrome_options.add_argument('--headless')
#chrome_options.add_argument('--disable-dev-shm-usage')

driver = webdriver.Chrome('/opt/supbot/chromedriver', chrome_options=chrome_options)

driver.implicitly_wait(5)

try:
    logging.info("Open administrative panel...")
    driver.get("http://tinder.com/")


    # """ Authorization Process """
    logging.info("Authenticating...")

    driver.execute_script("(function installFakeGeolocationCode() {var timerId = null; if (!navigator.geolocation) {return; } navigator.geolocation.getCurrentPosition_ = navigator.geolocation.getCurrentPosition; navigator.geolocation.watchPosition_ = navigator.geolocation.watchPosition; navigator.geolocation.clearWatch_ = navigator.geolocation.clearWatch; navigator.geolocation.clearWatch = function (id) {window.clearInterval(id); }; function sendBackResponse (successCb, errorCb, options, jsonedResponse) {var response = JSON.parse(jsonedResponse); if (response.isEnabled) {successCb({ 'coords': { 'latitude': response.lat, 'longitude': response.lng, 'accuracy': response.accuracy }, 'timestamp': new Date().getTime() }); } else {navigator.geolocation.getCurrentPosition_(successCb, errorCb, options); } } function createGetFakePosition(successCb, errorCb, options) {return function () {var position_content = '{\"isEnabled\":true,\"lat\":25.0548704,\"lng\":121.6311445,\"accuracy\":1}'; var interval = setInterval(function() {clearInterval(interval); sendBackResponse(successCb, errorCb, options, position_content); }, 200 /* ms */); }; }; navigator.geolocation.getCurrentPosition = function (cb1, cb2, options) {var getFakePosition = createGetFakePosition(cb1, cb2, options); getFakePosition(); }; navigator.geolocation.watchPosition = function (cb1, cb2, options) {var getFakePosition = createGetFakePosition(cb1, cb2, options); getFakePosition(); if (timerId) {window.clearInterval(timerId); } timerId = window.setInterval(getFakePosition, 5 * 1000); return timerId; }; })();")

    driver.find_element_by_xpath("//body/div[2]/div/div/div[2]/div/div[3]/div[2]/button").click()

    # """ Pasting Phone number """
    logging.info("Past phone number")
    driver.find_element_by_name("phone_number").send_keys(PHONE)
    
    driver.find_element_by_xpath("//body/div[2]/div/div/div[2]/button").click()
    
    # """ Getting SMS code """
    while True:
        Elements = driver.find_elements_by_xpath("//body/div[2]/div/div/div[2]/button")
        if not Elements:
            break
        else:
            logging.info("Get SMS code from input")
            CODE = input("SMS code?\n")
            sleep(3)
            # """ Pasting SMS code """
            logging.info("Pasting SMS code")
            driver.find_element_by_xpath("//body/div[2]/div/div/div[2]/div[3]/input").send_keys(CODE)
            driver.find_element_by_xpath("//body/div[2]/div/div/div[2]/button").click()
            
    # """ Setting location """
    logging.info("Execute javascript for fakePosition")
    driver.execute_script("(function installFakeGeolocationCode() {var timerId = null; if (!navigator.geolocation) {return; } navigator.geolocation.getCurrentPosition_ = navigator.geolocation.getCurrentPosition; navigator.geolocation.watchPosition_ = navigator.geolocation.watchPosition; navigator.geolocation.clearWatch_ = navigator.geolocation.clearWatch; navigator.geolocation.clearWatch = function (id) {window.clearInterval(id); }; function sendBackResponse (successCb, errorCb, options, jsonedResponse) {var response = JSON.parse(jsonedResponse); if (response.isEnabled) {successCb({ 'coords': { 'latitude': response.lat, 'longitude': response.lng, 'accuracy': response.accuracy }, 'timestamp': new Date().getTime() }); } else {navigator.geolocation.getCurrentPosition_(successCb, errorCb, options); } } function createGetFakePosition(successCb, errorCb, options) {return function () {var position_content = '{\"isEnabled\":true,\"lat\":25.0548704,\"lng\":121.6311445,\"accuracy\":1}'; var interval = setInterval(function() {clearInterval(interval); sendBackResponse(successCb, errorCb, options, position_content); }, 200 /* ms */); }; }; navigator.geolocation.getCurrentPosition = function (cb1, cb2, options) {var getFakePosition = createGetFakePosition(cb1, cb2, options); getFakePosition(); }; navigator.geolocation.watchPosition = function (cb1, cb2, options) {var getFakePosition = createGetFakePosition(cb1, cb2, options); getFakePosition(); if (timerId) {window.clearInterval(timerId); } timerId = window.setInterval(getFakePosition, 5 * 1000); return timerId; }; })();")

    # """ Skip banners and apply geolocation request """
    logging.info("Skip banners")
    # """ Step 1 """
    try:
        driver.find_element_by_xpath("//body/div/div/span/div/div[2]/div/div/div/div/div[3]/button").click()
    except NoSuchElementException:
        logging.info("Skip step 1")
        sleep(1)
        pass

    # """ Step 2 """
    try:
        driver.find_element_by_xpath("//body/div/div/span/div/div[2]/div/div/main/div/div[3]/button").click()
    except NoSuchElementException:
        logging.info("Skip step 2")
        sleep(1)
        pass

    # """ Setting location """
    logging.info("Execute javascript for fakePosition")
    driver.execute_script("(function installFakeGeolocationCode() {var timerId = null; if (!navigator.geolocation) {return; } navigator.geolocation.getCurrentPosition_ = navigator.geolocation.getCurrentPosition; navigator.geolocation.watchPosition_ = navigator.geolocation.watchPosition; navigator.geolocation.clearWatch_ = navigator.geolocation.clearWatch; navigator.geolocation.clearWatch = function (id) {window.clearInterval(id); }; function sendBackResponse (successCb, errorCb, options, jsonedResponse) {var response = JSON.parse(jsonedResponse); if (response.isEnabled) {successCb({ 'coords': { 'latitude': response.lat, 'longitude': response.lng, 'accuracy': response.accuracy }, 'timestamp': new Date().getTime() }); } else {navigator.geolocation.getCurrentPosition_(successCb, errorCb, options); } } function createGetFakePosition(successCb, errorCb, options) {return function () {var position_content = '{\"isEnabled\":true,\"lat\":25.0548704,\"lng\":121.6311445,\"accuracy\":1}'; var interval = setInterval(function() {clearInterval(interval); sendBackResponse(successCb, errorCb, options, position_content); }, 200 /* ms */); }; }; navigator.geolocation.getCurrentPosition = function (cb1, cb2, options) {var getFakePosition = createGetFakePosition(cb1, cb2, options); getFakePosition(); }; navigator.geolocation.watchPosition = function (cb1, cb2, options) {var getFakePosition = createGetFakePosition(cb1, cb2, options); getFakePosition(); if (timerId) {window.clearInterval(timerId); } timerId = window.setInterval(getFakePosition, 5 * 1000); return timerId; }; })();")

    # """ Step 3 """
    try:
        driver.find_element_by_xpath("//body/div/div/span/div/div[2]/div/div/div[3]/button").click()
    except NoSuchElementException:
        logging.info("Skip step 3")
        sleep(1)
        pass

    # """ Step 4 """
    try:
        driver.find_element_by_xpath("//body/div/div/span/div/div[2]/div/div/div[3]/button").click()
    except NoSuchElementException:
        logging.info("Skip step 4")
        sleep(1)
        pass


    
    
    while True:
        sleep(5)

except Exception as e:
    logging.exception(e)
    driver.quit()


